part of 'week_data_bloc.dart';

/// Week data events
abstract class WeekDataEvent extends Equatable {
  const WeekDataEvent();
}

/// Event when there is new device data
class NewWeekDataEvent extends WeekDataEvent {
  final DeviceEvent deviceEvent;

  const NewWeekDataEvent({required this.deviceEvent});

  @override
  List<Object?> get props => [deviceEvent];
}

/// Event when a new week is selected
class NewWeekSelectedEvent extends WeekDataEvent {
  final DateTime newYear;
  final int newWeek;

  const NewWeekSelectedEvent({required this.newYear, required this.newWeek});

  @override
  List<Object?> get props => [newYear, newWeek];
}
