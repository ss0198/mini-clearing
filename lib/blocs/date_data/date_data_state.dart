part of 'date_data_bloc.dart';

/// State of date data bloc
class DateDataState extends Equatable {
  // The selected date
  final DateTime selectedDate;

  // The device data for the selected date
  final List<DeviceData> deviceData;

  const DateDataState({
    required this.selectedDate,
    required this.deviceData,
  });

  @override
  List<Object> get props => [selectedDate, deviceData];
}
