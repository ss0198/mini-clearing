import 'package:flutter_bloc/flutter_bloc.dart';

part 'top_navigation_event.dart';
part 'top_navigation_state.dart';

/// Bloc for top navigation
class TopNavigationBloc extends Bloc<TopNavigationEvent, TopNavigationState> {
  TopNavigationBloc() : super(TopNavigationState.day) {
    on<TopNavigationEvent>((event, emit) {
      switch (event) {
        case TopNavigationEvent.switchToDay:
          emit(TopNavigationState.day);
          break;
        case TopNavigationEvent.switchToWeek:
          emit(TopNavigationState.week);
          break;
        case TopNavigationEvent.switchToMonth:
          emit(TopNavigationState.month);
          break;
      }
    });
  }
}
