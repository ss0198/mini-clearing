import 'dart:async';
import 'dart:math';

import 'package:bloc_test/bloc_test.dart';
import 'package:mini_clearing/blocs/date_data/date_data_bloc.dart';
import 'package:mini_clearing/blocs/device_events/device_events_bloc.dart';
import 'package:mini_clearing/blocs/month_data/month_data_bloc.dart';
import 'package:mini_clearing/blocs/week_data/week_data_bloc.dart';
import 'package:mini_clearing/model/device_event.dart';
import 'package:mini_clearing/services/new_file_reader.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

/// Mock New File Reader
class MockNewFileReader extends Mock implements NewFileReader {}

/// Mock Date Data Bloc
class MockDateDataBloc extends MockBloc<DateDataEvent, DateDataState>
    implements DateDataBloc {}

/// Mock Week Data Bloc
class MockWeekDataBloc extends MockBloc<WeekDataEvent, WeekDataState>
    implements WeekDataBloc {}

/// Mock Month Data Bloc
class MockMonthDataBloc extends MockBloc<MonthDataEvent, MonthDataState>
    implements MonthDataBloc {}

void main() {
  group('DeviceEventsBloc', () {
    final Random random = Random(1000);

    // Stream of device event csv strings
    final Stream<String> newFileReaderData = Stream.periodic(
      const Duration(milliseconds: 20),
      (_) {
        final deviceEvent = DeviceEvent.random(random).toCsvString();
        //print(deviceEvent);
        return deviceEvent;
      },
    ).take(100).asBroadcastStream();

    late NewFileReader newFileReader;

    late DateDataBloc dateDataBloc;
    late WeekDataBloc weekDataBloc;
    late MonthDataBloc monthDataBloc;

    setUp(() {
      registerFallbackValue(
          NewDateDataEvent(deviceEvent: DeviceEvent.random(random)));
      registerFallbackValue(
          NewWeekDataEvent(deviceEvent: DeviceEvent.random(random)));
      registerFallbackValue(
          NewMonthDataEvent(deviceEvent: DeviceEvent.random(random)));

      newFileReader = MockNewFileReader();
      when(() => newFileReader.newFileReader()).thenAnswer(
        (_) => newFileReaderData,
      );

      dateDataBloc = MockDateDataBloc();
      weekDataBloc = MockWeekDataBloc();
      monthDataBloc = MockMonthDataBloc();
    });

    blocTest(
      'Verify that newFileReader is called once',
      build: () => DeviceEventsBloc(
        newFileReader: newFileReader,
        dateDataBloc: dateDataBloc,
        weekDataBloc: weekDataBloc,
        monthDataBloc: monthDataBloc,
      ),
      act: (bloc) => bloc.add(DeviceEventsInit()),
      verify: (_) {
        verify(() => newFileReader.newFileReader()).called(1);
      },
    );

    blocTest('New File triggers events for date, week, month data bloc',
        build: () => DeviceEventsBloc(
              newFileReader: newFileReader,
              dateDataBloc: dateDataBloc,
              weekDataBloc: weekDataBloc,
              monthDataBloc: monthDataBloc,
            ),
        act: (bloc) => bloc.add(DeviceEventsInit()),
        verify: (_) async {
          // Wait until all device data events are finished
          Completer<void> complete = Completer();
          newFileReaderData.listen((event) {}, onDone: (() {
            complete.complete();
          }));
          await expectAsync0(() => complete.future)();

          verify(() => dateDataBloc.add(any())).called(100);
          verify(() => weekDataBloc.add(any())).called(100);
          verify(() => monthDataBloc.add(any())).called(100);
        });
  });
}
