import 'dart:collection';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/model/device_data.dart';
import 'package:mini_clearing/model/device_event.dart';

part 'month_data_event.dart';
part 'month_data_state.dart';

/// Bloc for handling data of devices on month level
class MonthDataBloc extends Bloc<MonthDataEvent, MonthDataState> {
  // Month -> Device -> Data
  final Map<DateTime, Map<String, DeviceData>> monthData = HashMap();

  static DateTime monthOnly(DateTime dateTime) {
    return DateTime(dateTime.year, dateTime.month);
  }

  MonthDataBloc()
      : super(MonthDataState(
          selectedMonth: monthOnly(DateTime.now()),
          deviceData: const [],
        )) {
    on<NewMonthDataEvent>(
      (event, emit) {
        // Get month (ignore time)
        final DeviceEvent deviceEvent = event.deviceEvent;
        final DateTime month = monthOnly(deviceEvent.eventTime);

        // Get month
        Map<String, DeviceData> deviceDataForMonth = monthData.putIfAbsent(
          month,
          () => HashMap(), // Create new HashMap if not existent
        );

        // Get device
        if (deviceDataForMonth.containsKey(deviceEvent.deviceId)) {
          // Update value
          deviceDataForMonth[deviceEvent.deviceId] = DeviceData(
            deviceName: deviceEvent.deviceId,
            fieldY: deviceDataForMonth[deviceEvent.deviceId]!.fieldY +
                deviceEvent.fieldY,
          );
        } else {
          // Initialize value
          deviceDataForMonth[deviceEvent.deviceId] = DeviceData(
            deviceName: deviceEvent.deviceId,
            fieldY: deviceEvent.fieldY,
          );
        }

        // If currently selected date, update state
        if (month == state.selectedMonth) {
          emit(
            MonthDataState(
              selectedMonth: month,
              deviceData: monthData[month]?.values.toList() ?? [],
            ),
          );
        }
      },
    );

    // On new month selection
    on<NewMonthSelectedEvent>((event, emit) {
      final DateTime newMonth = monthOnly(event.newMonth);

      emit(
        MonthDataState(
          selectedMonth: newMonth,
          deviceData: monthData[newMonth]?.values.toList() ?? [],
        ),
      );
    });
  }
}
