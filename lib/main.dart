import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_acrylic/flutter_acrylic.dart' as flutter_acrylic;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/blocs/date_data/date_data_bloc.dart';
import 'package:mini_clearing/blocs/device_events/device_events_bloc.dart';
import 'package:mini_clearing/blocs/month_data/month_data_bloc.dart';
import 'package:mini_clearing/blocs/top_navigation/top_navigation_bloc.dart';
import 'package:mini_clearing/blocs/week_data/week_data_bloc.dart';
import 'package:mini_clearing/services/new_file_reader.dart';
import 'package:mini_clearing/ui/components/top_navigation.dart';
import 'package:system_theme/system_theme.dart';
import 'package:window_manager/window_manager.dart';

/// Checks if the current environment is a desktop environment.
bool get isDesktop {
  if (kIsWeb) return false;
  return [
    TargetPlatform.windows,
    TargetPlatform.linux,
    TargetPlatform.macOS,
  ].contains(defaultTargetPlatform);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // if it's not on the web, windows or android, load the accent color
  if (!kIsWeb &&
      [
        TargetPlatform.windows,
        TargetPlatform.android,
      ].contains(defaultTargetPlatform)) {
    SystemTheme.accentColor.load();
  }

  if (isDesktop) {
    await flutter_acrylic.Window.initialize();
    await WindowManager.instance.ensureInitialized();
    windowManager.waitUntilReadyToShow().then((_) async {
      await windowManager.setTitleBarStyle(
        TitleBarStyle.normal,
        windowButtonVisibility: true,
      );
      await windowManager.setSize(const Size(755, 545));
      await windowManager.setMinimumSize(const Size(350, 600));
      await windowManager.center();
      await windowManager.show();
      //await windowManager.setPreventClose(true);
      await windowManager.setSkipTaskbar(false);
    });
  }

  runApp(const MiniClearing());
}

class MiniClearing extends StatelessWidget {
  const MiniClearing({super.key});

  @override
  Widget build(BuildContext context) {
    return FluentApp(
        debugShowCheckedModeBanner: false,
        title: 'Mini-Clearing',
        home: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) => TopNavigationBloc(),
            ),
            BlocProvider(
              create: (context) => DateDataBloc(),
              lazy: false,
            ),
            BlocProvider(
              create: (context) => WeekDataBloc(),
              lazy: false,
            ),
            BlocProvider(
              create: (context) => MonthDataBloc(),
              lazy: false,
            ),
            BlocProvider(
              create: (context) => DeviceEventsBloc(
                newFileReader: NewFileReader(),
                dateDataBloc: context.read<DateDataBloc>(),
                weekDataBloc: context.read<WeekDataBloc>(),
                monthDataBloc: context.read<MonthDataBloc>(),
              )..add(
                  DeviceEventsInit(),
                ),
              lazy: false,
            ),
          ],
          child: const TopNavigation(),
        ));
  }
}
