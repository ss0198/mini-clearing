import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/blocs/week_data/week_data_bloc.dart';
import 'package:mini_clearing/ui/components/device_table.dart';

class WeekPage extends StatelessWidget {
  const WeekPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final WeekDataBloc weekDataBloc = context.watch<WeekDataBloc>();
    final WeekDataState weekDataState = weekDataBloc.state;

    return Column(
      children: [
        DatePicker(
          showDay: false,
          showMonth: false,
          selected: weekDataState.selectedYear,
          onChanged: (DateTime newYear) => weekDataBloc.add(
            NewWeekSelectedEvent(
              newYear: newYear,
              newWeek: weekDataState.selectedWeek, // Keep week in year
            ),
          ),
        ),
        ComboBox(
          value: weekDataState.selectedWeek,
          onChanged: (int? newWeek) => weekDataBloc.add(
            NewWeekSelectedEvent(
              newYear: weekDataState.selectedYear, // Keep year
              newWeek: newWeek!,
            ),
          ),
          items: List.generate(
              53,
              (index) => ComboBoxItem(
                    value: index,
                    child: Text(index.toString()),
                  )),
        ),
        DeviceTable(deviceData: weekDataState.deviceData),
      ],
    );
  }
}
