part of 'month_data_bloc.dart';

/// State of month data bloc
class MonthDataState extends Equatable {
  // The selected month
  final DateTime selectedMonth;

  // The device data for the selected month
  final List<DeviceData> deviceData;

  const MonthDataState({
    required this.selectedMonth,
    required this.deviceData,
  });

  @override
  List<Object> get props => [selectedMonth, deviceData];
}
