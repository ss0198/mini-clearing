import 'dart:math';

import 'package:csv/csv.dart';
import 'package:intl/intl.dart';

/// A specific event of a device
class DeviceEvent {
  static DateFormat dateFormat = DateFormat("yyyy-MM-dd kk:mm:ss");

  final int eventId;
  final DateTime eventTime;
  final String deviceId;
  final String eventName;
  final String eventType;
  final int fieldX;
  final int fieldY;
  final String fieldZ;

  const DeviceEvent({
    required this.eventId,
    required this.eventTime,
    required this.deviceId,
    required this.eventName,
    required this.eventType,
    required this.fieldX,
    required this.fieldY,
    required this.fieldZ,
  });

  /// Transforms a list with data from csv decoding, to a device event
  static DeviceEvent fromList(List<dynamic> csvList) => DeviceEvent(
        eventId: parseCsvInt(csvList[0]),
        eventTime: dateFormat.parse((csvList[1] as String).trim()),
        deviceId: parseCsvString(csvList[2]),
        eventName: parseCsvString(csvList[3]),
        eventType: parseCsvString(csvList[4]),
        fieldX: parseCsvInt(csvList[5]),
        fieldY: parseCsvInt(csvList[6]),
        fieldZ: parseCsvString(csvList[7]),
      );

  static DeviceEvent random(Random random) => DeviceEvent(
        eventId: random.nextInt(100),
        eventTime: DateTime(
          random.nextInt(4) + 2020,
          random.nextInt(12) + 1,
          random.nextInt(28) + 1,
          random.nextInt(24),
          random.nextInt(60),
          random.nextInt(60),
        ),
        deviceId: String.fromCharCodes(
          List.generate(
            random.nextInt(4) + 1,
            (_) => random.nextInt(122 - 48) + 48,
          ),
        ),
        eventName: String.fromCharCodes(
          List.generate(
            random.nextInt(2) + 1,
            (_) => random.nextInt(122 - 48) + 48,
          ),
        ),
        eventType: String.fromCharCodes(
          List.generate(
            random.nextInt(2) + 1,
            (_) => random.nextInt(122 - 48) + 48,
          ),
        ),
        fieldX: random.nextInt(10),
        fieldY: random.nextInt(10),
        fieldZ: String.fromCharCodes(
          List.generate(
            random.nextInt(2) + 1,
            (_) => random.nextInt(122 - 48) + 48,
          ),
        ),
      );

  static int parseCsvInt(dynamic value) {
    if (value is int) {
      return value;
    } else if (value is String) {
      return int.parse(value.trim());
    } else {
      throw Exception('Error: provided value is not an int');
    }
  }

  static String parseCsvString(dynamic value) {
    if (value is String) {
      return value;
    } else if (value is int) {
      return value.toString();
    } else {
      throw Exception('Error: provided value is not a string');
    }
  }

  String toCsvString() {
    return const ListToCsvConverter().convert([
      [
        eventId,
        eventTime,
        deviceId,
        eventName,
        eventType,
        fieldX,
        fieldY,
        fieldZ,
      ]
    ]);
  }

  @override
  String toString() {
    return 'DeviceEvent{eventId: $eventId, eventTime: $eventTime, deviceId: $deviceId, eventName: $eventName, eventType: $eventType, fieldX: $fieldX, fieldY: $fieldY, fieldZ: $fieldZ}';
  }
}
