# mini_clearing

This application reads device events as csv files from the folder `csv-data` and displays real-time statistics grouped by date, week or month.