import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/blocs/top_navigation/top_navigation_bloc.dart';
import 'package:mini_clearing/ui/pages/date.dart';
import 'package:mini_clearing/ui/pages/month.dart';
import 'package:mini_clearing/ui/pages/week.dart';

class TopNavigation extends StatelessWidget {
  const TopNavigation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TopNavigationBloc topNavigationBloc =
        context.watch<TopNavigationBloc>();
    final int currentTabIdx = topNavigationBloc.state.index;

    return NavigationView(
      pane: NavigationPane(
        selected: currentTabIdx,
        onChanged: (index) => topNavigationBloc.add(
          TopNavigationEvent.values[index],
        ),
        displayMode: PaneDisplayMode.top,
        items: [
          PaneItem(
            icon: const Icon(FluentIcons.date_time),
            title: const Text('Day'),
            body: const DatePage(),
          ),
          PaneItem(
            icon: const Icon(FluentIcons.date_time),
            title: const Text('Week'),
            body: const WeekPage(),
          ),
          PaneItem(
            icon: const Icon(FluentIcons.date_time),
            title: const Text('Month'),
            body: const MonthPage(),
          ),
        ],
      ),
    );
  }
}
