import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/blocs/month_data/month_data_bloc.dart';
import 'package:mini_clearing/ui/components/device_table.dart';

class MonthPage extends StatelessWidget {
  const MonthPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final MonthDataBloc monthDataBloc = context.watch<MonthDataBloc>();
    final MonthDataState monthDataState = monthDataBloc.state;

    return Column(
      children: [
        DatePicker(
          showDay: false,
          selected: monthDataState.selectedMonth,
          onChanged: (DateTime newMonth) => monthDataBloc.add(
            NewMonthSelectedEvent(
              newMonth: newMonth,
            ),
          ),
        ),
        DeviceTable(deviceData: monthDataState.deviceData),
      ],
    );
  }
}
