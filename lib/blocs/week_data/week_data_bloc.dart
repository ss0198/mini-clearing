import 'dart:collection';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/model/device_data.dart';
import 'package:mini_clearing/model/device_event.dart';

part 'week_data_event.dart';
part 'week_data_state.dart';

/// Bloc for handling data of devices on week level
class WeekDataBloc extends Bloc<WeekDataEvent, WeekDataState> {
  // Year -> Week -> Device -> Data
  final Map<DateTime, Map<int, Map<String, DeviceData>>> weekData = HashMap();

  static DateTime getYear(DateTime dateTime) {
    return DateTime(dateTime.year);
  }

  static int getWeekNumber(DateTime dateTime) {
    return (dateTime.difference(DateTime(DateTime.now().year, 1, 1)).inDays / 7)
        .ceil();
  }

  WeekDataBloc()
      : super(WeekDataState(
          selectedYear: getYear(DateTime.now()),
          selectedWeek: getWeekNumber(DateTime.now()),
          deviceData: const [],
        )) {
    on<NewWeekDataEvent>(
      (event, emit) {
        // Get year and week (ignore time)
        final DeviceEvent deviceEvent = event.deviceEvent;
        final DateTime year = getYear(deviceEvent.eventTime);
        final int week = getWeekNumber(deviceEvent.eventTime);

        // Get year
        Map<int, Map<String, DeviceData>> deviceDataForYear =
            weekData.putIfAbsent(
          year,
          () => HashMap(), // Create new HashMap if not existent
        );

        // Get week
        Map<String, DeviceData> deviceDataForWeek =
            deviceDataForYear.putIfAbsent(
          week,
          () => HashMap(), // Create new HashMap if not existent
        );

        // Get device
        if (deviceDataForWeek.containsKey(deviceEvent.deviceId)) {
          // Update value
          deviceDataForWeek[deviceEvent.deviceId] = DeviceData(
            deviceName: deviceEvent.deviceId,
            fieldY: deviceDataForWeek[deviceEvent.deviceId]!.fieldY +
                deviceEvent.fieldY,
          );
        } else {
          // Initialize value
          deviceDataForWeek[deviceEvent.deviceId] = DeviceData(
            deviceName: deviceEvent.deviceId,
            fieldY: deviceEvent.fieldY,
          );
        }

        // If currently selected week, update state
        if (week == state.selectedWeek && year == state.selectedYear) {
          emit(
            WeekDataState(
              selectedYear: year,
              selectedWeek: week,
              deviceData: weekData[year]?[week]?.values.toList() ?? [],
            ),
          );
        }
      },
    );

    // On new week selection
    on<NewWeekSelectedEvent>((event, emit) {
      emit(
        WeekDataState(
          selectedYear: event.newYear,
          selectedWeek: event.newWeek,
          deviceData:
              weekData[event.newYear]?[event.newWeek]?.values.toList() ?? [],
        ),
      );
    });
  }
}
