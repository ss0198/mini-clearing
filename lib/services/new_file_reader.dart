import 'dart:io';

import 'package:flutter/foundation.dart';

class NewFileReader {
  Stream<String> newFileReader() => Directory.fromUri(
        Uri.file(
          defaultTargetPlatform == TargetPlatform.windows
              ? "${Directory.current.path}\\csv-data"
              : "${Directory.current.path}/csv-data",
          windows: defaultTargetPlatform == TargetPlatform.windows,
        ),
      )
          .watch(
        events: FileSystemEvent.create,
        recursive: false,
      )
          .asyncMap(
        (FileSystemEvent element) {
          // Read new file
          String filePath = element.path;
          return File.fromUri(Uri.file(filePath)).readAsString();
        },
      );
}
