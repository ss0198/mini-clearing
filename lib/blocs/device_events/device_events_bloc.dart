import 'package:csv/csv.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/blocs/date_data/date_data_bloc.dart';
import 'package:mini_clearing/blocs/month_data/month_data_bloc.dart';
import 'package:mini_clearing/blocs/week_data/week_data_bloc.dart';
import 'package:mini_clearing/model/device_event.dart';
import 'package:mini_clearing/services/new_file_reader.dart';

part 'device_events_event.dart';
part 'device_events_state.dart';

/// Bloc for reading events from csv files
class DeviceEventsBloc extends Bloc<DeviceEventsEvent, DeviceEventsState> {
  DeviceEventsBloc({
    required NewFileReader newFileReader,
    required DateDataBloc dateDataBloc,
    required WeekDataBloc weekDataBloc,
    required MonthDataBloc monthDataBloc,
  }) : super(DeviceEventsInitial()) {
    on<DeviceEventsInit>((event, emit) {
      // Listen to new files in data directory
      newFileReader
          .newFileReader()
          .map(
            (String event) {
              // Decode csv
              final List<List<dynamic>> csvData =
                  const CsvToListConverter().convert(event);
              final List<DeviceEvent> deviceEvents =
                  csvData.map((data) => DeviceEvent.fromList(data)).toList();

              return deviceEvents;
            },
          )
          .expand((x) => x) // Split to individual rows;
          .forEach((deviceEvent) {
            // Notify data blocs
            dateDataBloc.add(NewDateDataEvent(deviceEvent: deviceEvent));
            weekDataBloc.add(NewWeekDataEvent(deviceEvent: deviceEvent));
            monthDataBloc.add(NewMonthDataEvent(deviceEvent: deviceEvent));
          });
    });
  }
}
