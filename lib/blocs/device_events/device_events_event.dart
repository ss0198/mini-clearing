part of 'device_events_bloc.dart';

abstract class DeviceEventsEvent extends Equatable {
  const DeviceEventsEvent();
}

class DeviceEventsInit extends DeviceEventsEvent {
  @override
  List<Object?> get props => [];
}
