part of 'month_data_bloc.dart';

/// Month data events
abstract class MonthDataEvent extends Equatable {
  const MonthDataEvent();
}

/// Event when there is new device data
class NewMonthDataEvent extends MonthDataEvent {
  final DeviceEvent deviceEvent;

  const NewMonthDataEvent({required this.deviceEvent});

  @override
  List<Object?> get props => [deviceEvent];
}

/// Event when a new month is selected
class NewMonthSelectedEvent extends MonthDataEvent {
  final DateTime newMonth;

  const NewMonthSelectedEvent({required this.newMonth});

  @override
  List<Object?> get props => [newMonth];
}
