import 'dart:io';

import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/blocs/date_data/date_data_bloc.dart';
import 'package:mini_clearing/ui/components/device_table.dart';

class DatePage extends StatelessWidget {
  const DatePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final DateDataBloc dateDataBloc = context.watch<DateDataBloc>();
    final DateDataState dateDataState = dateDataBloc.state;

    return Column(
      children: [
        SelectableText(
          'Reading data from "${Directory.current.path}/csv-data"',
        ),
        DatePicker(
          selected: dateDataState.selectedDate,
          onChanged: (DateTime newDate) => dateDataBloc.add(
            NewDateSelectedEvent(
              newDate: newDate,
            ),
          ),
        ),
        DeviceTable(deviceData: dateDataState.deviceData),
      ],
    );
  }
}
