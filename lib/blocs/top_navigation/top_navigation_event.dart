part of 'top_navigation_bloc.dart';

enum TopNavigationEvent {
  switchToDay,
  switchToWeek,
  switchToMonth,
}
