part of 'week_data_bloc.dart';

/// State of week data bloc
class WeekDataState extends Equatable {
  // The selected year
  final DateTime selectedYear;

  // The selected week
  final int selectedWeek;

  // The device data for the selected week
  final List<DeviceData> deviceData;

  const WeekDataState({
    required this.selectedYear,
    required this.selectedWeek,
    required this.deviceData,
  });

  @override
  List<Object> get props => [
        selectedYear,
        selectedWeek,
        deviceData,
      ];
}
