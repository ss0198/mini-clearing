import 'dart:collection';

import 'package:equatable/equatable.dart';
import 'package:fluent_ui/fluent_ui.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mini_clearing/model/device_data.dart';
import 'package:mini_clearing/model/device_event.dart';

part 'date_data_event.dart';
part 'date_data_state.dart';

/// Bloc for handling data of devices on date level
class DateDataBloc extends Bloc<DateDataEvent, DateDataState> {
  // Date -> Device -> Data
  final Map<DateTime, Map<String, DeviceData>> dateData = HashMap();

  DateDataBloc()
      : super(DateDataState(
          selectedDate: DateUtils.dateOnly(DateTime.now()),
          deviceData: const [],
        )) {
    on<NewDateDataEvent>(
      (event, emit) {
        // Get date (ignore time)
        final DeviceEvent deviceEvent = event.deviceEvent;
        final DateTime date = DateUtils.dateOnly(deviceEvent.eventTime);

        // Get day
        Map<String, DeviceData> deviceDataForDay = dateData.putIfAbsent(
          date,
          () => HashMap(), // Create new HashMap if not existent
        );

        // Get device
        if (deviceDataForDay.containsKey(deviceEvent.deviceId)) {
          // Update value
          deviceDataForDay[deviceEvent.deviceId] = DeviceData(
            deviceName: deviceEvent.deviceId,
            fieldY: deviceDataForDay[deviceEvent.deviceId]!.fieldY +
                deviceEvent.fieldY,
          );
        } else {
          // Initialize value
          deviceDataForDay[deviceEvent.deviceId] = DeviceData(
            deviceName: deviceEvent.deviceId,
            fieldY: deviceEvent.fieldY,
          );
        }

        // If currently selected date, update state
        if (date == state.selectedDate) {
          emit(
            DateDataState(
              selectedDate: date,
              deviceData: dateData[date]?.values.toList() ?? [],
            ),
          );
        }
      },
    );

    // On new date selection
    on<NewDateSelectedEvent>((event, emit) {
      final DateTime newDate = DateUtils.dateOnly(event.newDate);

      emit(
        DateDataState(
          selectedDate: newDate,
          deviceData: dateData[newDate]?.values.toList() ?? [],
        ),
      );
    });
  }
}
