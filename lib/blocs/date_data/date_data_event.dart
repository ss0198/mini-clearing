part of 'date_data_bloc.dart';

/// Date data events
abstract class DateDataEvent extends Equatable {
  const DateDataEvent();
}

/// Event when there is new device data
class NewDateDataEvent extends DateDataEvent {
  final DeviceEvent deviceEvent;

  const NewDateDataEvent({required this.deviceEvent});

  @override
  List<Object?> get props => [deviceEvent];
}

/// Event when a new date is selected
class NewDateSelectedEvent extends DateDataEvent {
  final DateTime newDate;

  const NewDateSelectedEvent({required this.newDate});

  @override
  List<Object?> get props => [newDate];
}
