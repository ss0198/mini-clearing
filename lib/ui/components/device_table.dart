import 'package:flutter/material.dart';
import 'package:mini_clearing/model/device_data.dart';

class DeviceTable extends StatelessWidget {
  final List<DeviceData> deviceData;

  const DeviceTable({Key? key, required this.deviceData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: const [
        DataColumn(label: Text('Device Name')),
        DataColumn(label: Text('FieldY'))
      ],
      rows: deviceData
          .map(
            (device) => DataRow(
              cells: [
                DataCell(Text(device.deviceName)),
                DataCell(
                  Text(device.fieldY.toString()),
                ),
              ],
            ),
          )
          .toList(),
    );
  }
}
