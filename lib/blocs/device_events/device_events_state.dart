part of 'device_events_bloc.dart';

abstract class DeviceEventsState extends Equatable {
  const DeviceEventsState();
}

class DeviceEventsInitial extends DeviceEventsState {
  @override
  List<Object> get props => [];
}
