import 'package:equatable/equatable.dart';

/// Data of one device
class DeviceData extends Equatable {
  final String deviceName;
  final int fieldY;

  const DeviceData({required this.deviceName, required this.fieldY});

  @override
  List<Object?> get props => [deviceName, fieldY];

  @override
  String toString() {
    return 'DeviceData{deviceName: $deviceName, fieldY: $fieldY}';
  }
}
